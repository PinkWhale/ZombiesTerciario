﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Zombies2018.Sprites
{
    public class Zombie : Sprite
    {
        public Zombie()
        {
            Image = Game1.TheGame.Content.Load<Texture2D>("Images/male");
            Rectangulo = new Rectangle(Game1.TheGame.GraphicsDevice.Viewport.Width - 80, random.Next(Game1.TheGame.GraphicsDevice.Viewport.Height - 80), 80, 80);
            Color = Color.White;
        }

        public override void Update(GameTime gameTime)
        {
            int x = Rectangulo.X;
            x -= 2;

            Rectangulo = new Rectangle(x, Rectangulo.Y, Rectangulo.Width, Rectangulo.Height);
        }
    }
}
