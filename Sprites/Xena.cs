﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombies2018.Sprites
{
    public class Xena : Sprite // eso significa heredo de la clase Sprite
    {
        public Xena()
        {
            Image = Game1.TheGame.Content.Load<Texture2D>("Images/nave");

            Rectangulo = new Rectangle(50, 50, 80, 80);
            Color = Color.White;
        }

        //Para mover el bicho
        public override void Update(GameTime gameTime)
        {
            int x, y;
            x = Rectangulo.X;
            y = Rectangulo.Y;

            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                x-=5;//Rectangulo.X++;
            Rectangulo = new Rectangle(x, y, Rectangulo.Width, Rectangulo.Height);

            if (Keyboard.GetState().IsKeyDown(Keys.Right))
                x+=5;//Rectangulo.X++;
            Rectangulo = new Rectangle(x, y, Rectangulo.Width, Rectangulo.Height);

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
                y-=5;//Rectangulo.X++;
            Rectangulo = new Rectangle(x, y, Rectangulo.Width, Rectangulo.Height);

            if (Keyboard.GetState().IsKeyDown(Keys.Down))
                y+=5;//Rectangulo.X++;

            if (x > Game1.TheGame.GraphicsDevice.Viewport.Width - Rectangulo.Width)
                x = Game1.TheGame.GraphicsDevice.Viewport.Width - Rectangulo.Width;
            else if (x < 0)
                x = 0;

            if (y > Game1.TheGame.GraphicsDevice.Viewport.Height - Rectangulo.Height)
                y = Game1.TheGame.GraphicsDevice.Viewport.Height - Rectangulo.Height;
            else if (y < 0)
                y = 0;

            Rectangulo = new Rectangle(x, y, Rectangulo.Width, Rectangulo.Height);


        }
    }
}
